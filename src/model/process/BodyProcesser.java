package model.process;

import javax.media.j3d.BoundingBox;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Vector3f;

import model.load.J3DLoader;
import model.struct.Body;
import model.struct.Point3d;

import com.sun.j3d.loaders.Scene;

/**
 * @author zhouyun 2014-6-14����10:39:21
 */
public class BodyProcesser {

	public void test1(Body b1, Body b2, String location) {

		if (location.equals("UP")) {
			// b2.getCore_pos().y3d = b1.getCore_pos().y3d+b1.getHeight()+b2.getHeight();
			// b2.getCore_pos().x3d = b1.getCore_pos().x3d;
			// b2.getCore_pos().z3d = b1.getCore_pos().z3d;
			b2.setCore_pos(new Point3d(b1.getCore_pos().x3d,
					b1.getCore_pos().y3d + b1.getHeight() + b2.getHeight(), b1.getCore_pos().z3d));
		}
		if (location.equals("RIGHT")) {
			b2.getCore_pos().y3d = b1.getCore_pos().y3d - b1.getHeight() + b2.getHeight();
			b2.getCore_pos().x3d = b1.getCore_pos().x3d + b1.getLength() + b2.getLength();
			b2.getCore_pos().z3d = b1.getCore_pos().z3d;
		}
		if (location.equals("LEFT")) {
			b2.getCore_pos().y3d = b1.getCore_pos().y3d - b1.getHeight() + b2.getHeight();
			b2.getCore_pos().x3d = b1.getCore_pos().x3d
					- (b1.getLength() + b2.getLength());
			b2.getCore_pos().z3d = b1.getCore_pos().z3d;
		}
		if (location.equals("FRONT")) {
			b2.getCore_pos().x3d = b1.getCore_pos().x3d;
			b2.getCore_pos().y3d = b1.getCore_pos().y3d - b1.getHeight() + b2.getHeight();
			b2.getCore_pos().z3d = b1.getCore_pos().z3d + (b1.getWidth() + b2.getWidth());
		}
		if (location.equals("BACK")) {
			b2.getCore_pos().x3d = b1.getCore_pos().x3d;
			b2.getCore_pos().y3d = b1.getCore_pos().y3d - b1.getHeight() + b2.getHeight();
			b2.getCore_pos().z3d = b1.getCore_pos().z3d - (b1.getWidth() + b2.getWidth());
		}
		if (location.equals("DOWN")) {
			// B在A下面的情况,还没写
		}
	}

}
