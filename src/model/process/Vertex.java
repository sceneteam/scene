package model.process;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author zhouyun 2014-6-6����8:40:52
 */
public class Vertex {

	public static float[] testVertex(String vertexName) {
		int n = 0;

		float[] min = new float[3];
		float[] max = new float[3];
		float[][] aVertexCoor = new float[100000][3];

		float[] xyzLen = new float[3]; // XYZ方向上的长度

		try {
			FileReader fr = new FileReader(vertexName);
			BufferedReader br = new BufferedReader(fr);
			String a;

			while ((a = br.readLine()) != null) {

				if (a.indexOf('v') == 0 && a.indexOf('t') != 1
						&& a.indexOf('n') != 1) {

					n++;
					int alen = a.length();
					String aStr = null;
					aStr = a.substring(2, alen); // �����ȥ��ʶV���ո��������ֵ��ɵ��ַ�
					// System.out.println(aStr);
					
					String[] aVertex = aStr.trim().split(" ");

					for (int i = 0; i < 3; i++) {
						aVertexCoor[n][i] = Float.parseFloat(aVertex[i]);
					}
				}

			}
			fr.close();
			br.close();
		} catch (IOException e) {
			System.out.println(e);
		}


		// 计算长宽高
		for (int j = 0; j < 3; j++) {
			min[j] = aVertexCoor[1][j];
			max[j] = aVertexCoor[1][j];
		}
		for (int i = 2; i <= n; i++) {
			for (int j = 0; j < 3; j++) {
				if (aVertexCoor[i][j] > max[j])
					max[j] = aVertexCoor[i][j];
				if (aVertexCoor[i][j] < min[j])
					min[j] = aVertexCoor[i][j];
			}
		}

		for (int i = 0; i < 3; i++) {
			xyzLen[i] = max[i] - min[i];
		}


		float maxLength = xyzLen[0]; 
		if (xyzLen[1] > maxLength)
			maxLength = xyzLen[1];
		if (xyzLen[2] > maxLength)
			maxLength = xyzLen[2];

		// float ratio=maxLength/2;
		
		float ratio = 2;//返回的长宽高的比例（1/ratio）
		for (int i = 0; i < 3; i++) {
			xyzLen[i] /= ratio;
		}
		//System.out.println( xyzLen[0]+"  "+xyzLen[1]+"  "+xyzLen[2]);
		return xyzLen;
	}

}
