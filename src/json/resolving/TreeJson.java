package json.resolving;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import model.process.BodyProcesser;
import model.struct.Body;

import com.alibaba.fastjson.*;

public class TreeJson {
	//static String ResPath = "E:\\workspace\\scene\\res\\";
	static String ResPath = "res\\";
	public void BFS(Vector<Body> bd, BodyProcesser bp, String filename)
			throws FileNotFoundException  {

		// fastjson 解析
		JSONReader jsonReader = new JSONReader(new FileReader(
				"JSON_IN\\" + filename));
		String jsString = jsonReader.readString();

		//把string转换成JSONObject
		JSONObject jsonObject = JSONObject.parseObject(jsString);
		JSONObject jsObj = new JSONObject();
		Queue<Body> queue = new LinkedList<Body>();
		
		Queue<JSONObject> QjsonObj = new LinkedList<JSONObject>();
		//Queue<JSONObject> qjsonObj = new LinkedList<JSONObject>();
		//分别是子节点nodeArray和子节点所在方向oriArray
		JSONArray nodeArray = new JSONArray();
		JSONArray oriArray = new JSONArray();

		String headString = jsonObject.getString("iD");
		System.out.println(headString);

		Body rootBody = new Body();
		rootBody.testBody(ResPath + headString + ".obj");
		
		//把根节点加入队列
		queue.offer(rootBody);
		QjsonObj.offer(jsonObject);
		
		Body parent = new Body();
		while ( !queue.isEmpty() ) {
			//每次循环就加入一个节点的所有子节点，parent是此次循环的父节点
			parent = queue.poll();
			jsonObject = QjsonObj.poll();
			
			System.out.print(jsonObject.getString("iD")+"  ");
			System.out.print(parent.getCore_pos());
			
			nodeArray = JSONArray.parseArray(jsonObject.getString("node"));
			oriArray = JSONArray.parseArray(jsonObject.getString("orientations"));
			for (int i = 0; i < nodeArray.size(); i++) {
				Body tmpBody = new Body();
				String name = nodeArray.getJSONObject(i).getString("iD");
				System.out.print(name + "  ");
				
				tmpBody.testBody(ResPath + name + ".obj");
				bp.test1(parent, tmpBody, oriArray.getString(i));
				System.out.print(tmpBody.getCore_pos());
				queue.offer(tmpBody);
				QjsonObj.offer(nodeArray.getJSONObject(i));
			}
			System.out.println();
			bd.add(new Body(parent));
		}		

	}
	
}
